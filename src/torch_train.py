import os
import time
import mnist_loader
import torch
import torch.optim as op
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torch_network import Network
import snapshots
import log_db
import numpy as np

def main():
    train_network('torch_test', {
        "rate": 3.0,
        # TODO josh: set manual seed
    }, 30)

transforms = {
}

def train_network(group, hyper, n_epochs):
    flags = []
    for k, v in enumerate(hyper):
        if k in transforms:
            v = transforms[k](v)
        flags.append('{}={}'.format(k, v))
    name = 'torch.'+'.'.join(flags)
    loader = snapshots.ModelLoader(name)
    print('loading test data')
    training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
    print('training on {} data points'.format(len(training_data)))
    print(f'loading latest snapshot for {name}')
    snapshot, checkpoint = loader.load_latest()
    if checkpoint is None:
        print(f'no snapshot found, starting from scratch')
        net = Network()
    else:
        # TODO josh: implement loading snapshots
        print('found snapshot {generation} from {timestamp}'.format(**snapshot))
        net = network.deserialize(model, cost_fn=cost)
    saved_epoch = 0 if checkpoint is None else checkpoint['epoch']
    o = op.SGD(net.parameters(), lr=0.001, momentum=0.3)
    writer = SummaryWriter()
    data_size = 10_000
    batch_size = 1
    batcher = DataLoader(training_data[:data_size],
        batch_size=batch_size)
    for epoch in range(saved_epoch, 10000, 1):
        t1 = time.time()
        running_loss = 0
        n_batches = 0
        for batch in batcher:
            a, b = batch
            a = a.squeeze()
            b = b.squeeze()
            o.zero_grad()
            guess = net(a)
            loss = ((guess - b)**2).sum()
            to_v = lambda x: f"{x.item():.7f}"
            if os.getenv('VERBOSE')=='1':
                print('G ', '|'.join(map(to_v, guess.squeeze())))
                print('B ', '|'.join(map(to_v, b.squeeze())))
                print('=='*8)
            loss.backward()
            o.step()
            running_loss += loss.item()
            n_batches += 1
        t2 = time.time()
        avg_loss = running_loss/n_batches
        print(epoch,
            f"l={avg_loss:.3f} time={t2-t1:.1f}")
        writer.add_scalar('Loss', running_loss, epoch)
        writer.add_scalar('Epoch time', t2-t1, epoch)
    writer.flush()

def cost_plain(x, y):
    return x-y

def cost_double(x, y):
    return 2*(x-y)

# NOTE: this is being used as a derivative so this only makes sense if the
# original cost function was 1/3x**3
def cost_square(x, y):
    return (x-y)**2

cost_functions = {
    'plain': cost_plain,
    'double': cost_double,
    'square': cost_square,
}

if __name__ == '__main__':
    main()
