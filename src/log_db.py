import sys
import math
import json
import sqlite3
from pathlib import Path
import tkinter
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
matplotlib.use('TkAgg')

db_versions = [
    ["create table meta (k string primary key, v string);"],
    [
        "create table models (id string primary key, parameters int, spec string)",
        """
        create table training_epochs (
            ts int,
            model string,
            epoch int,
            train_ms int,
            training_loss numeric,
            training_success_rate numeric,
            validation_loss numeric,
            validation_success_rate numeric
        );
        """,
    ],
    [
        "alter table training_epochs rename to model_epochs",
    ],
    [
        "alter table models add column model_group text",
    ],
]
db_versions = list(zip(range(0, len(db_versions)), db_versions))

def dict_factory(cursor, row):
    names = [col[0] for col in cursor.description]
    return {key: value for key, value in zip(names, row)}

def open(path):
    db = sqlite3.connect(path)
    db.row_factory = dict_factory
    c = db.cursor()
    version = -1 
    table = c.execute("""
        select name
        from sqlite_master
        where type = 'table' and name = 'meta'
    """).fetchone()
    if table is not None:
        version = int(c.execute("""
        select v as version
        from meta
        where k = 'version'
        """).fetchone()['version'])
    for version, statements in db_versions[version+1:]:
        print(f'Building table schema version {version}')
        c.execute("begin transaction")
        for statement in statements:
            print('- statement: {}'.format(preview(statement)))
            c.execute(statement)
        c.execute("""
            insert into meta (k, v)
            values ('version', ?)
            on conflict (k) do update set v = excluded.v
        """, (str(version),))
        db.commit()
    return db

def model(db, group, name, hyper):
    parameters = 0
    layers = hyper['layers']
    for a, b in zip(layers[:-1], layers[1:]):
        parameters += a*b+b
    db.execute("""
        insert into models (model_group, id, parameters, spec)
        values (?, ?, ?, ?)
        on conflict (id) do nothing
    """, (group, name, parameters, json.dumps(hyper)))
    db.commit()


def model_epoch(db, name, epoch, train_ms, training_loss, training_success_rate,
                validation_loss, validation_success_rate):
    res = db.execute("""
        insert into model_epochs (ts, model, epoch, train_ms, training_loss,
            training_success_rate, validation_loss, validation_success_rate)
        values (current_timestamp, ?, ?, ?, ?, ?, ?, ?)
    """, (
        name,
        epoch,
        train_ms,
        training_loss,
        training_success_rate,
        validation_loss,
        validation_success_rate
    ))
    db.commit()

def list_models(db, group):
    ms = list(db.execute("""
        select id, parameters, spec as hyper
        from models
        where model_group = ?
    """, (group,)))
    return [{**m, 'hyper': json.loads(m['hyper'])} for m in ms]

def list_groups(db):
    records = list(db.execute("""
        select distinct model_group
        from models
        where model_group is not null
    """))
    return [r['model_group'] for r in records]


def preview(v):
    return " ".join([x.strip() for x in v.strip()[:100].split("\n")])[:50]

def rm(path):
    Path(path).unlink()

def learning_history(db, model):
    return list(db.execute("""
        select ts, epoch, training_loss, validation_loss
        from model_epochs
        where model = ?
        order by epoch asc
    """, (model['id'],)))

def normalize(v):
    if v['training_loss']==-1:
        v['training_loss'] = None
    if v['validation_loss']==-1:
        v['validation_loss'] = None
    return v

def series(values, y_key, x_key='epoch'):
    if not len(values):
        return {'x': [], 'y': []}
    xs = [v[x_key] for v in values]
    ys = [v[y_key] for v in values]
    pairs = [(x, y) for x, y in zip(xs, ys) if y is not None]
    xs, ys = zip(*pairs)
    return {'x': list(xs), 'y': list(ys)}

def show_rates_graph(db, group):
    models = list_models(db, group)
    histories = [[normalize(x) for x in learning_history(db, model)]
        for model in models]
    all_training = [series(history, 'training_loss') for history in histories]
    all_validation = [series(history, 'validation_loss') for history in histories]
    fig = plt.figure()
    ax = fig.add_subplot()
    ax.set_yscale('log')
    ax.grid(True)
    ax.set_xlabel("Epochs (training rounds)")
    ax.set_ylabel("Loss (magnitude of error)")
    def series_name(model):
        # layers = "-".join([str(n) for n in model['hyper']['layers']])
        return f"cost={json.dumps(model['hyper']['layers'])}"
    for model, s, color in zip(models, all_training, make_colors()):
        line, = ax.plot(s['x'], s['y'], color=color)
        line.set_label("Training "+series_name(model))
    for model, s, color in zip(models, all_validation, make_colors()):
        line, = ax.plot(s['x'], s['y'], color=color, marker='x')
        line.set_label("Testing "+series_name(model))
    ax.legend()
    plt.show()

def make_colors():
    for c in colors.TABLEAU_COLORS:
        yield c
    raise Exception('Ran out of colors :(')

if __name__=='__main__':
    path = '../training.db'
    db = open(path)
    if len(sys.argv)==2:
        show_rates_graph(db, sys.argv[1])
    else:
        for g in list_groups(db):
            print('-', g)
