from pathlib import Path
import pickle
import time

class ModelLoader:
    def __init__(self, name):
        self.name = name
        self.base = Path(f'../models/{self.name}/')

    def load_latest(self):
        snapshots = self.list_snapshots()
        if not len(snapshots):
            return None, None
        snapshot = sorted(snapshots,
            key=lambda x: x['generation'], reverse=True)[0]
        return snapshot, self.load(snapshot)

    def list_snapshots(self):
        if not self.base.is_dir():
            return []
        return [parse_snapshot(self.name, p)
            for p in self.base.iterdir()]

    def load(self, snapshot):
        with open(snapshot['path'], mode='rb') as f:
            return pickle.load(f)

    def save(self, model):
        self.base.mkdir(parents=True, exist_ok=True)
        snapshot = {
            'name': self.name,
            'generation': model['generation'],
            'timestamp': round(time.time()),
        }
        snapshot['path'] = self.base.joinpath(
            '{}.{}.checkpoint'.format(snapshot['timestamp'],
            snapshot['generation']))
        with open(snapshot['path'], mode='wb') as f:
            pickle.dump(model, f)
        return snapshot

def parse_snapshot(name, path):
    t, g, *rest = path.name.split('.')
    return {
        'name': name,
        'timestamp': t,
        'generation': int(g),
        'path': path,
    }
