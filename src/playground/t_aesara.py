import os
import aesara as A
import aesara.tensor as at
import numpy as np

A.config.assert_no_cpu_op = 'raise'
print('device', A.config.device)

v = at.dscalar('v')
sigmoid = A.function([v], 1 / (1+at.exp(-v)))

x = at.dscalar('x')
y = x**2
gy = at.grad(y, x)
print(A.pp(gy))
print(A.function([x], gy))
