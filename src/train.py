import time
from PIL import Image
import mnist_loader
import network
import snapshots
import log_db
import numpy as np

def main():
    db = log_db.open('../training.db')
    for fn in [1]:
        train_network('cost_functions', {
            "rate": 3.0,
            "batch_size": 10,
            "layers": [784, 30, 10],
            "seed": 0,
            # "training_size": 10**n,
            "cost_fn": 'plain',
        }, 30, db)

def train_network(group, hyper, n_epochs, db):
    s_layers = [str(v) for v in hyper['layers']]
    flags = ['layers={}'.format("-".join(s_layers))]
    for k in ['rate', 'batch_size', 'seed', 'subsample_training', 'training_size', 'cost_fn']:
        try:
            flags.append('{}={}'.format(k, hyper[k]))
        except KeyError: pass
    name = 'nn.'+'.'.join(flags)
    log_db.model(db, group, name, hyper)
    loader = snapshots.ModelLoader(name)
    print('loading test data')
    training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
    if 'subsample_training' in hyper:
        n = round(len(training_data) *
                  hyper['subsample_training'])
        training_data = training_data[0:n]
    if 'training_size' in hyper:
        training_data = training_data[0:hyper['training_size']]
    print('training on {} data points'.format(len(training_data)))
    print(f'loading latest snapshot for {name}')
    snapshot, model = loader.load_latest()
    if 'seed' in hyper:
        np.random.seed(hyper['seed'])
    cost = cost_functions[hyper['cost_fn']]
    if model is None:
        print(f'no snapshot found, starting from scratch')
        net = network.Network(hyper['layers'], cost_fn=cost)
    else:
        print('found snapshot {generation} from {timestamp}'.format(**snapshot))
        net = network.deserialize(model, cost_fn=cost)
    counter = 0 if snapshot is None else snapshot['generation']
    epochs = []
    for _ in range(net.generation, n_epochs, 1):
        t1 = time.time()
        stats = net.SGD(training_data, hyper['batch_size'], hyper['rate'])
        t2 = time.time()
        train_ms = (t2-t1)*1000//1
        counter += 1
        test_stats = None
        if counter%6==0:
            test_stats = net.test(test_data)
        if test_stats is not None:
            print((
                'Epoch {}: t={:.2f}s train_loss={:.4f} '
                'test_loss={:.4f} test_success={:.2f}%'
            ).format(stats['generation'], t2-t1, stats['loss'], test_stats['loss'],
                     test_stats['success']*100))
            epochs.append((stats['generation'], train_ms,
                stats['loss'], None, test_stats['loss'], test_stats['success']))
        else:
            print((
                'Epoch {}: t={:.2f}s train_loss={:.4f}'
            ).format(stats['generation'], t2-t1, stats['loss']))
            epochs.append((stats['generation'], train_ms,
                stats['loss'], None, None, None))
        # CONTINUE: show/refresh graphs of loss curve for training and test
        # loss during run
        if test_stats is not None:
            for epoch in epochs:
                log_db.model_epoch(db, name, *epoch)
            snapshot = loader.save(net.serialize())
            print('saved snapshot {}'.format(snapshot['path']))
            epochs = []

def cost_plain(x, y):
    return x-y

def cost_double(x, y):
    return 2*(x-y)

# NOTE: this is being used as a derivative so this only makes sense if the
# original cost function was 1/3x**3
def cost_square(x, y):
    return (x-y)**2

cost_functions = {
    'plain': cost_plain,
    'double': cost_double,
    'square': cost_square,
}

if __name__ == '__main__':
    main()
