"""
network.py
~~~~~~~~~~

A module to implement the stochastic gradient descent learning
algorithm for a feedforward neural network.  Gradients are calculated
using backpropagation.  Note that I have focused on making the code
simple, easily readable, and easily modifiable.  It is not optimized,
and omits many desirable features.
"""

#### Libraries
# Standard library
import random
import time

# Third-party libraries
import numpy as np


class Network(object):
    def __init__(self, layers, cost_fn):
        """The list ``sizes`` contains the number of neurons in the
        respective layers of the network.  For example, if the list
        was [2, 3, 1] then it would be a three-layer network, with the
        first layer containing 2 neurons, the second layer 3 neurons,
        and the third layer 1 neuron.  The biases and weights for the
        network are initialized randomly, using a Gaussian
        distribution with mean 0, and variance 1.  Note that the first
        layer is assumed to be an input layer, and by convention we
        won't set any biases for those neurons, since biases are only
        ever used in computing the outputs from later layers."""
        self.generation = 0
        self.layers = layers
        self.biases = [np.random.randn(y, 1) for y in layers[1:]]
        self.weights = [np.random.randn(y, x)
                        for x, y in zip(layers[:-1], layers[1:])]
        self.cost_fn = cost_fn
    def serialize(self):
        return {
            'generation': self.generation,
            'layers': self.layers,
            'weights': self.weights,
            'biases': self.biases,
        }

    def feedforward(self, a):
        """Return the output of the network if ``a`` is input."""
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w, a)+b)
        return a

    def SGD(self, training_data, mini_batch_size, rate):
        """Train the neural network using mini-batch stochastic
        gradient descent.  The ``training_data`` is a list of tuples
        ``(x, y)`` representing the training inputs and the desired
        outputs.
        """
        n = len(training_data)
        random.shuffle(training_data)
        mini_batches = [
            training_data[k:k+mini_batch_size]
            for k in range(0, n, mini_batch_size)]
        batch_loss = []
        batch_success = []
        for mini_batch in mini_batches:
            stats = self.update_mini_batch(mini_batch, rate)
            batch_loss.append(stats['loss'])
            batch_success.append(stats['success'])
        self.generation += 1
        return {
            'generation': self.generation,
            'loss': sum(batch_loss)/len(batch_loss),
            'success': sum(batch_success)/len(batch_success),
        }

    def update_mini_batch(self, mini_batch, rate):
        """Update the network's weights and biases by applying
        gradient descent using backpropagation to a single mini batch.
        The ``mini_batch`` is a list of tuples ``(x, y)``, and ``rate``
        is the learning rate."""
        b_gradient = [np.zeros(b.shape) for b in self.biases]
        w_gradient = [np.zeros(w.shape) for w in self.weights]
        batch_rate = rate/len(mini_batch)
        for x, y in mini_batch:
            delta_b_gradient, delta_w_gradient, loss = self.backprop(x, y)
            b_gradient = [nb+dnb for nb, dnb in zip(b_gradient, delta_b_gradient)]
            w_gradient = [nw+dnw for nw, dnw in zip(w_gradient, delta_w_gradient)]
        self.weights = [w-batch_rate*dw for w, dw in zip(self.weights, w_gradient)]
        self.biases = [b-batch_rate*db for b, db in zip(self.biases, b_gradient)]
        return {'loss': loss, 'success': -1}

    def backprop(self, x, y):
        """Return a tuple ``(b_gradient, w_gradient, loss)`` representing the
        gradient for the cost function C_x and the previous cost of this sample. 
        ``b_gradient`` and ``w_gradient`` have the same shape as self.biases and self.weights.
        """
        b_gradient = [np.zeros(b.shape) for b in self.biases]
        w_gradient = [np.zeros(w.shape) for w in self.weights]
        # feedforward
        state = x
        activations = [x] # list to store all the activations, layer by layer
        zs = [] # list to store all the z vectors, layer by layer
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, state)+b
            zs.append(z)
            state = sigmoid(z)
            activations.append(state)
        loss = sum(self.cost_fn(state, y).squeeze())
        # backward pass
        delta = self.cost_fn(state, y) * sigmoid_prime(zs[-1])
        b_gradient[-1] = delta
        w_gradient[-1] = np.dot(delta, activations[-2].transpose())
        # Note that the variable l in the loop below is used a little
        # differently to the notation in Chapter 2 of the book.  Here,
        # l = 1 means the last layer of neurons, l = 2 is the
        # second-last layer, and so on.  It's a renumbering of the
        # scheme in the book, used here to take advantage of the fact
        # that Python can use negative indices in lists.
        for l in range(2, len(self.layers)):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * sp
            b_gradient[-l] = delta
            w_gradient[-l] = np.dot(delta, activations[-l-1].transpose())
        return (b_gradient, w_gradient, loss)

    def test(self, test_data):
        """Return the number of test inputs for which the neural
        network outputs the correct result. Note that the neural
        network's output is assumed to be the index of whichever
        neuron in the final layer has the highest activation."""
        activations = []
        for x, y in test_data:
            activations.append((self.feedforward(x), y))
        loss = avg([magnitude(x-expected(y)) for x, y in activations])
        success = avg([int(np.argmax(x)==y) for x, y in activations])
        # BUG maybe: it doesn't seem like this goes over 1.0.
        # If this is not buggy this should max out at 10,no?
        return {'loss': loss, 'success': success}

#### Miscellaneous functions
def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

def sigmoid_prime(z):
    """Derivative of the sigmoid function."""
    return sigmoid(z)*(1-sigmoid(z))

def deserialize(model, cost_fn):
    n = Network(model['layers'], cost_fn)
    n.generation = model['generation']
    n.weights = model['weights']
    n.biases = model['biases']
    return n

def expected(n):
    v = np.zeros((10, 1))
    v[n, 0] = 1
    return v

def magnitude(vec):
    return sum(abs(v) for v in vec.squeeze())

def avg(arr):
    return sum(arr)/len(arr)
