import torch
import torch.nn as nn
import torch.nn.functional as F


class Network(nn.Module):
    def __init__(self):
        super(Network, self).__init__()
        self.l = nn.Linear(784, 30)
        self.l2 = nn.Linear(30, 10)

    def forward(self, x):
        x = F.sigmoid(self.l(x))
        x = F.sigmoid(self.l2(x))
        return x
