import sys
import pickle
from PIL import Image
import mnist_loader
import network
from pathlib import Path

def usage():
    print('Usage: python extract.py <model>')

def main():
    print('loading')
    training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
    try:
        model_path = sys.argv[1]
    except IndexError:
        return usage()
    with open(model_path, 'rb') as f:
        net = network.deserialize(pickle.load(f))
    right_img = image_dir('right')
    wrong_img = image_dir('wrong')
    for i, (pixels, exp) in enumerate(test_data):
        out = g2i(net.feedforward(pixels))
        tag = f"image={i}.png guess={out} real={exp}"
        if out==exp:
            print(tag, 'RIGHT')
            save_image(right_img, i, pixels)
        else:
            print(tag, 'WRONG')
            save_image(wrong_img, i, pixels)

def g2i(arr):
    guesses = [round(x[0]) for x in arr]
    for i, guess in enumerate(guesses):
        if guess:
            return i
    return -1

def image_dir(bucket):
    base = Path(f'../images/{bucket}')
    base.mkdir(exist_ok=True)
    for f in base.iterdir():
        f.unlink(missing_ok=True)
    return base

W = 28
H = 28
def save_image(bucket, name, pixels):
    img = Image.new('L', (W, H))
    px = img.load()
    pixels = [int(255-(v*255)) for v in pixels.transpose()[0]]
    for i, v in enumerate(pixels):
        px[i%W, i//H] = v
    with open(bucket.joinpath(f"{name}.png"), 'wb') as f:
        img.save(f)

if __name__ == '__main__':
    main()
